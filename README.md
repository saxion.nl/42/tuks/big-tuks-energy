## Installation
### Live sass compiler
- Install the extension in Visual Studio Code (Live Sass Compiler, by glenn2223). 
- Open the .scss file in the css map
- Make sure the extension is watching your file, by enabling it in the blue bar on the bottom of Visual Studio Code (watch scss).
- Now only make changes in the style.scss file, after saving, this will automatically generate a style.css file for you.